import cv2
import torch
import numpy as np
import albumentations as A

class SegDataset:
    def __init__(self, x_paths, augmentation, num_classes):
        self.x_paths = x_paths
        self.y_paths = [x.replace("image", "mask").replace(".jpg", "_lab.png") for x in self.x_paths]
        self.augmentation = augmentation
        self.num_classes = num_classes
        self.train_trans = A.Compose([
            A.HorizontalFlip(p=0.5),
            A.VerticalFlip(p=0.5),
            A.ShiftScaleRotate(shift_limit=0.2, scale_limit=0.2, rotate_limit=30, p=0.5),
            A.ColorJitter(brightness=0.3, contrast=0.3, saturation=0.2, hue=0.1, p=0.5),
            A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))
            ])
        self.val_trans = A.Compose([
            A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))
            ])

    def __len__(self):
        return len(self.x_paths)
    
    def get_newMask(self, mask):
        mask = torch.as_tensor(mask[:,:,0], dtype=torch.int64)
        return torch.moveaxis(torch.nn.functional.one_hot(mask, num_classes=self.num_classes), -1, 0)

    def __getitem__(self, index):
        image = cv2.imread(self.x_paths[index])
        kernel = np.ones((2,2),np.uint8)
        image = cv2.bilateralFilter(image, 5, 75, 75)
        image = cv2.erode(cv2.dilate(image, kernel, iterations=2), kernel, iterations=1)
        mask = cv2.imread(self.y_paths[index])
       
        if self.augmentation:
            transformed = self.train_trans(image=image, mask=mask)
        else:
            transformed = self.val_trans(image=image, mask=mask)
        
        image = transformed["image"]
        mask = self.get_newMask(transformed["mask"])
        image = np.transpose(image, (2, 0, 1))
        
        return image, mask, index