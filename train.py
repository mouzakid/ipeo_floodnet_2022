import os
import copy
import time
import numpy as np
import sklearn
from tqdm.auto import tqdm
from collections import defaultdict
import torch
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.utils.data import DataLoader
import torch.nn.functional as F
import segmentation_models_pytorch


def dice_loss(pred, target, smooth = 1e-5):
    '''
    Calculate Dice Loss
    '''
    pred = pred.contiguous()
    target = target.contiguous()    
    intersection = (pred * target).sum(dim=2).sum(dim=2)
    loss = (1 - ((2. * intersection + smooth) / (pred.sum(dim=2).sum(dim=2) + target.sum(dim=2).sum(dim=2) + smooth)))
    
    return loss.mean()

def calc_loss(pred, target, metrics, bce_weight=0.5):
    '''
    Calculate loss, pixel accuracy, and MIoU in the training and validation stages

    Returns
    ==================
    hist_df: dataframe of the number of pixels in each class
    '''
    target_np = target.data.cpu().numpy()
    target_class = np.argmax(target_np, axis=1).flatten()
    pred = torch.sigmoid(pred)
    pred_np = pred.data.cpu().numpy()    
    pred_class = np.argmax(pred_np, axis=1).flatten()

    bce = F.binary_cross_entropy_with_logits(pred, target.to(torch.float32))
    dice = dice_loss(pred, target)
    loss = bce * bce_weight + dice * (1 - bce_weight)

    MIoU = np.mean(sklearn.metrics.jaccard_score(target_class, pred_class, average=None))
    pixel_acc = sklearn.metrics.accuracy_score(target_class, pred_class)
    
    metrics['bce'] += bce.data.cpu().numpy() * target.size(0)
    metrics['dice'] += dice.data.cpu().numpy() * target.size(0)
    metrics['loss'] += loss.data.cpu().numpy() * target.size(0)
    metrics['pixel accuracy'] += pixel_acc * target.size(0)
    metrics['MIoU'] += MIoU * target.size(0)
    # By multiplying a metric with the number of samples in a batch,
    # and dividing by the total number of samples in function "print_metrics", 
    # the output is a weighted average of the metric over batches.

    return loss

def print_metrics(metrics, epoch_samples, phase):
    outputs = []
    for k in metrics.keys():
        outputs.append("{}: {:4f}".format(k, metrics[k] / epoch_samples))
    print("{}: {}".format(phase, ", ".join(outputs)))

def train_model(unique_name, batch_size, learning_rate, num_epochs, num_classes,
                encoder_name, encoder_depth, encoder_weights, decoder_channels,
                train_dataset, valid_dataset, model_dir, device, scheduler=False):
    '''
    Train and validate the model
    
    Parameters
    ==================
    unique_name: self-defined name of the model
    batch_size: batch size of the dataloader, default is 8 in evaluation.ipynb
    learning_rate: learning rate, default is 0.001
    num_epochs: number of epochs, default is 100
    encoder_name: name of the encoder, default is "efficientnet-b3"
    decoder_name: name of the decoder, default is "deeplabv3plus"
    encoder_weights: name of the pre-trained weights, default is "imagenet"
    train_dataset: training dataset initialized from SegDataset
    valid_dataset: validation dataset initialized from SegDataset
    model_dir: directory where the model weights will be saved
    device: cuda/cpu
    scheduler: True/False, whether or not to apply learning rate scheduler
    '''
    for lr in learning_rate:
        for bs in batch_size:
            print("__"*80)
            print("__"*80)
            print(f"name: {unique_name} LR: {lr} BS: {bs}")
            print("__"*80)
            print("__"*80)

            # os.makedirs(os.path.join(TENSORBOARD_DIR, f'{unique_name}-{lr}-{bs}'), exist_ok=True)
            # writer = SummaryWriter(log_dir=os.path.join(TENSORBOARD_DIR, f'{unique_name}-{lr}-{bs}'))
      
            best_loss = 1e10
            best_epoch = 0
            best_miou = 0

            train_data_loader = torch.utils.data.DataLoader(train_dataset, batch_size=bs, num_workers=0)
            valid_data_loader = torch.utils.data.DataLoader(valid_dataset, batch_size=bs, num_workers=0)
            dataloaders = {'train': train_data_loader, 'valid': valid_data_loader}

            model = segmentation_models_pytorch.DeepLabV3Plus(encoder_name=encoder_name, encoder_depth=encoder_depth, 
                    encoder_weights=encoder_weights, decoder_channels=decoder_channels, classes=num_classes)
            model = model.to(device)

            optimizer = optim.Adam(model.parameters(), lr=lr)
            # optimizer = optim.SGD(model.parameters(), lr=lr)

            if scheduler:
                scheduler = lr_scheduler.MultiStepLR(optimizer, milestones=[7, 20, 80], gamma=0.1)
            else:
                scheduler = None

            for epoch in range(1, num_epochs + 1):
                print('__' * 80)
                print('Epoch {}/{}'.format(epoch, num_epochs))
                print('__' * 80)

                since = time.time()

                for phase in ['train', 'valid']:
                    print(phase)
                    print("__"*20)
                    if phase == 'train':
                        model.train()
                    else:
                        model.eval()

                    metrics = defaultdict(float)
                    epoch_samples = 0

                    for batch_no, (inputs, labels, _) in enumerate(tqdm(dataloaders[phase])):
                        inputs = inputs.to(device)
                        labels = labels.to(device)
                        optimizer.zero_grad() # zero the parameter gradients

                        # forward
                        with torch.set_grad_enabled(phase == 'train'):
                            outputs = model(inputs)
                            loss = calc_loss(outputs, labels, metrics)
                        
                        # backward + optimize only if in training phase
                        if phase == 'train':
                            loss.backward()
                            optimizer.step()

                    # statistics
                    epoch_samples += inputs.size(0)

                    if scheduler and phase == "train":
                        scheduler.step()

                    print_metrics(metrics, epoch_samples, phase)
                    epoch_loss = metrics['loss'] / epoch_samples
                    epoch_miou = metrics['MIoU'] / epoch_samples
                    epoch_pixel_acc = metrics['pixel accuracy'] / epoch_samples

                    ## tensorboard writer
                    # writer.add_scalar(f'Loss/{phase}', epoch_loss, epoch)
                    # writer.add_scalar(f'MIoU/{phase}', epoch_miou, epoch)
                    # writer.add_scalar(f'Pixel accuracy/{phase}', epoch_pixel_acc, epoch)

                    # Model weights are saved when the highest validation MIoU is achieved
                    if phase == 'valid':
                        if epoch_miou > best_miou: 
                            best_miou = epoch_miou
                            best_epoch = epoch
                            best_model_wts = copy.deepcopy(model.state_dict())
                            print(f'Best miou: {best_miou:.4f} Epoch: {epoch}')
                        if epoch_loss < best_loss:
                            best_loss = epoch_loss

                

        print('Best val loss: {:4f}'.format(best_loss))
        # writer.close()
        PATH = os.path.join(model_dir, f'{unique_name}-ep_{best_epoch}-{lr}-{bs}.pt')
        torch.save(best_model_wts, PATH)
        
    return model