import cv2
import torch
import numpy as np
import torch
import sklearn
from sklearn import metrics
import matplotlib.pyplot as plt


def reverse_transform_mask(inp):
    '''
    Tranform predictions to the original size for visualization
    '''
    inp=inp.transpose((1, 2, 0))
    t_mask=np.argmax(inp,axis=2).astype('float32') 
    t_mask=cv2.resize(t_mask, dsize=(4000, 3000))
    kernel = np.ones((3,3),np.uint8)
    t_mask = cv2.erode(t_mask, kernel, iterations=1)
    
    return t_mask

def mask_one_hot(mask_path, num_classes):
    '''
    Transform ground truth masks to one-hot encoding for metric calculation

    Returns:
    ==================
    numpy array containing class labels
    '''
    mask = cv2.imread(mask_path)
    mask = torch.as_tensor(mask[:,:,0], dtype=torch.int64)
    mask = torch.moveaxis(torch.nn.functional.one_hot(mask, num_classes=num_classes), -1, 0)
    mask_np = mask.numpy()
  
    return np.argmax(mask_np, axis=0).flatten()

def calc_and_print_metrics(target_paths, pred_paths, classes_dict):
    '''
    Calculate and print metrics: pixel accuracy, MIoU, IoU for each class, confusion matrix

    Parameters
    ==================
    classes_dict: dictionary with class names as keys
    '''
    all_targets, all_preds = [], []
    for t_path in target_paths:
        target_class = mask_one_hot(t_path, len(classes_dict))
        all_targets.extend(target_class)
    for p_path in pred_paths:
        pred_np = np.load(p_path)
        pred_class = np.argmax(pred_np, axis=0).flatten()
        all_preds.extend(pred_class)

    IoU = sklearn.metrics.jaccard_score(all_targets, all_preds, average=None)
    MIoU = np.mean(IoU)
    pixel_acc = sklearn.metrics.accuracy_score(all_targets, all_preds)
    
    IoU_per_class = {}
    class_names = list(classes_dict.keys())
    class_union = list(sorted(set(all_targets).union(set(all_preds)))) 
    # class_union -> only classes that occur in prediction or ground truth will have IoU values
    assert len(class_union) == 10 # number of classes should be 10
    for i in range(len(class_union)):
        c = class_union[i]
        IoU_per_class[class_names[c]] = IoU_per_class.get(class_names[c], 0) + IoU[i]

    print("{}: {:4f}".format('Pixel accuracy', pixel_acc))
    print("{}: {:4f}".format('MIoU', MIoU))
    print('IoU for each class:')
    for key, val in IoU_per_class.items():
        print("{}: {:4f}".format(key, val))

    cm = metrics.confusion_matrix(all_targets, all_preds).astype(np.float32)
    fig, ax = plt.subplots(figsize=(13, 13))
    for i in range(cm.shape[0]):
        cm[i,:] = cm[i,:] / np.sum(cm[i,:])
        for j in range(cm.shape[1]):
            ax.text(x=j, y=i,s="{:.2E}".format(cm[i, j]), va='center', ha='center', size='x-large')
    
    ax.matshow(cm, cmap=plt.cm.Blues)
    ax.set_xticks(np.arange(len(class_names)))
    ax.set_yticks(np.arange(len(class_names)))
    ax.set_xticklabels(class_names, ha='left', rotation=30)
    ax.set_yticklabels(class_names)
    ax.tick_params(axis='both', which='major', labelsize=18)
    plt.xlabel('Predictions', fontsize=20)
    plt.ylabel('Ground Truth', fontsize=20)
    plt.show()