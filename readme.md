# IPEO 2022 Project | FloodNet: Segmentation of Images during a Flooding Event 

## Evaluation Repository
This repository contains the source code that was created in the framework of the project for the course "Image Processing for Earth Observations" at EPFL.
The dataset studied is FloodNet from [Rahnemoonfar et al, 2021], a high-resolution UAV imagery obtained during a flooding event caused by Hurricane Harvey. The baseline model used in this project, with a few variations, is based on the implementation of [Khose et al., 2021].

The repository is organised as below:
* dataset.py --> side script that processes the dataset
* evaluation.ipynb --> main script to run for the evaluation of the workflow
* readme.md --> this current file that you readme
* requirements.txt --> dependencies to be installed, please see below
* test.py --> side script with tools for the testing part
* train.py --> side script with tools for the training part

## Dataset
The dataset used is automatically downloaded in the main script, evaluation.ipynb, and it is saved locally in this current folder.
You can also find it available here: https://drive.google.com/file/d/1g08igaipgjGmbLSu12Xk3WXB6OYdRjVF .

## Setup
Please install the dependencies required in a new python environment following the steps below:
```
# create a local virtual environment in the venv folder
python -m venv venv
# activate this environment
source venv/bin/activate
# install requirements
pip install --upgrade pip
pip install jupyter
pip install -r requirements.txt
```

Start jupyter notebook in this environment and run the evaluation.ipynb notebook.
```
jupyter notebook
```

You can find details on the implementation in the report submitted together with this repository.

## Attention 
If a window named "Notebook validation failed" pops-up, please ignore it by closing the window. You should be able to continue running the script with no problem.

## References
Sahil Khose, Abhiraj Tiwari and Ankita Ghosh. ‘Semi-Supervised Classification and Segmentation on High Resolution Aerial Images’. In: arXiv preprint arXiv:2105.08655 (2021).

Maryam Rahnemoonfar et al. ‘Floodnet: A high resolution aerial imagery dataset for post flood scene understanding’. In: IEEE Access 9 (2021), pp. 89644–89654.